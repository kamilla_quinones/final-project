/**
 * Items that customers are going to order.
 * @author Kdq282
 *
 */

public class Items {
	//declare needed variables
		 String size,types,items;
		 static double smallDrinkPrice = .89, mediumDrinkPrice = 1.68, largeDrinkPrice= 1.99;
		 double totalPrice;
		 int numberOfItems =0;
		 static double chicken= 1.45, steak= 1.55, carnitas =1.55;
		static double caesar= 3.99, veggie= 3.99;
		 double tax = 0.05;
		static double Tacos = 4.00;
		static double Bowls= 5.00;
		static double Burritos= 5.00;
		static double Quesadillas= 3.00;
		static double Salads= 5.00;
		public static String [] SIZES = {"None","Small","Medium","Large"};
		public static String [] MEATS = {"chicken","steak","carnitas","caesar","veggie"};
		public static String [] ITEMS = {"tacos","bowls","burritos","quesadillas","salads"};
		public static double[] meatsPrices = {1.45, 1.55,1.55,3.99,3.99};
		public static double [] sizesPrices ={ 0, .89, 1.68, 1.99};
		public static double [] itemsPrices ={ 4.00, 5.00, 5.00, 3.00, 5.00};
		/**
		 * Size for drinks
		 * Types of meats
		 * Items to choose from
		 */
		public Items(){
			size= "Large";
			types="chicken,steak,carnitas,caesar,veggie";
		    items= "tacos,bowls,burritos,quesadillas,salads,drink";
		}
		public Items(String itemType){
			size="";
			types= itemType;
		}
		public Items(String typeItems, String sizeSel){
			size= sizeSel;
			types=typeItems;
			
		}
		public String getTypes(){
			return types;
			
		}
		public String getSize(){
			return size;
			
		}
		public String getItems(){
			return items;
		}
		/**
		 * set the tax
		 */
		public void setTax(){
			this.tax = tax;
		}
		//set small drink prices for drinks
		public void setSmallDrinkPrice(){
			 this.smallDrinkPrice = smallDrinkPrice;
		}
		//set medium drink prices for drinks
		public void setMediumDrinkPrice(){
		 this.mediumDrinkPrice = mediumDrinkPrice;
		}
		//set large drink prices for drinks
		public void setLargeDrinkPrice(){
			this.largeDrinkPrice = largeDrinkPrice;
		}
		//se tsize for drinks
		public void setSzie(){
			this.size = size;
		}
		//set types of meats for items.
		public void setTypes(){
			this.types = types;
		}
		//set what kind of items there is 
	    public void setItems(){
		 this.items = items;
	 }
	    //set tacos
		public void setTacos(double Tacos){
			this.Tacos= Tacos;
		}
	// set bowls
		public void setBowls(double Bowls){
			this.Bowls = Bowls;
		}
	//set bowls
		public void setBurritos(double Burritos){
			this.Burritos =Burritos;
		}
	// set quesadillas
		public void setQuesadillas (double Quesadillas){
			this.Quesadillas = Quesadillas;
		}
		//set salads
		public void setSalads(double Salads){
			this.Salads = Salads;
		}
		//set chicken
		public void setChicken(double chicken){
			this.chicken =chicken;
		}
		//set steak
		public void setSteak(double steak){
			this.steak = steak;
		}
		//set carnitas
		public void setCarnitas(double carnitas){
			this.carnitas= carnitas;
		}
		//set caesar
		public void setCaesar(double caesar){
			this.caesar= caesar;
		}
		// set veggie
		public void setVeggie(double veggie){
			this.veggie = veggie;
		}
		public double getTax(){
		return tax;
	}
		
		public double getSmallDrinkPrice(){
			return smallDrinkPrice;
		}
		
		public double getMediumDrinkPrice(){
			return mediumDrinkPrice;
		}
		
		public double getLargeDrinkPrice(){
			return largeDrinkPrice;
		}

		public double getTacos(){
			return Tacos;
		}

		public double getBowls(){
			return Bowls;
		}

		public double getBurritos(){
			return Burritos;
		}
		public double getQuesadillas(){
			return Quesadillas;
		}
		public double getSalads(){
			return Salads;
		}
		public double getChicken(){
			return chicken;
		}
		public double getSteak(){
			return steak;
		}
		public double getCarnitas(){
			return carnitas;
		}
		public double getCaesar(){
			return caesar;
		}

		public double getVeggie(){
			return veggie;
		}

		}


