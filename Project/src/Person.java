
public class Person {
	private String username;
	 private String password;
	 static int count;

	public static int getCount(){
	return ++count;
	} 
	 
	public Person(){
		super();
	}
	 
	 
	public Person(String username, String password) {
		this.username = username;
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String toString() {
		return "Person [username=" + username + ", password=" + password + "]";
	}
	 

}
