import java.awt.Component;

import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class Authentication extends JFrame {
	private static final int questionMessage = 0;
	private static int count;
	private Component frame;
	public static double[] meatsPrices = {1.45, 1.55,1.55,3.99,3.99};
	public static double [] sizesPrices ={ 0, .89, 1.68, 1.99};
	public static double [] itemsPrices ={ 4.00, 5.00, 5.00, 3.00, 5.00};
	

	//Constructor for the POS DEMO
	public Authentication() {
		setAuthentication();
	}
	public static int getCount() {
		return ++count;
	}

	
	
/**
 * setAuthentication in order to get in the existing account
 * @return 
 * @return 
 */
	public void setAuthentication() {

		final int Limit = 3;
		String correctusername = "Kamilla";
		String username;
		String Password;
		String correctpassword = "Dayana";
		String[] choices = { "Cashier", "Manager" };
		int countLimit = 0;
		boolean wrongAccountType = true;
		boolean successfulLogin = false;
		while (wrongAccountType) {
			String accountType;
			int accType = 0;

			accountType = (String) JOptionPane.showInputDialog(null,
					"Choose Account Type", "Account Menu", questionMessage,
					null, choices, choices[0]);
			if (accountType.equals(choices[0])) {
				accType = 0;
			}
			switch (accType) {
			case 0: {
				countLimit = 0;
				wrongAccountType = false;
				successfulLogin = false;
				// Part 1
				while (!successfulLogin) {
					username = JOptionPane.showInputDialog(null,
							"Enter Username ", null,
							JOptionPane.OK_CANCEL_OPTION);
					if (username.equalsIgnoreCase(correctusername)) {
						Password = JOptionPane.showInputDialog(null,
								"Enter Password", null,
								JOptionPane.OK_CANCEL_OPTION);
						if (Password.equalsIgnoreCase(correctpassword)) {
							JOptionPane.showConfirmDialog(null, "Welcome "
									+ accountType);
							successfulLogin = true;
						} else {
							// wrong password
							System.out.println("Fail Authentication. You have "
									+ (Limit - countLimit - 1)
									+ "trial(s) left.");
						}
					} else {
						// wrong username
						JOptionPane.showMessageDialog(null,
								"That username does not exist");
						System.out.println("Fail Authentication. You have "
								+ (Limit - countLimit - 1) + "trial(s) left.");
					}
					countLimit++;
					if (Limit == countLimit) {
						wrongAccountType = true;
						successfulLogin = true;

						
						
						JOptionPane.showMessageDialog(frame,
								"Contact Administrator.");
						break;
					}
					
					
		
				}
			}}}
			/**
			 * Selling Products of the items.
			 */
		//selling products
	int n = Integer.parseInt(JOptionPane.showInputDialog(null,"How many orders?"));
	String [] drinks = new String [n];
	String [] meats = new String [n];
	String [] items = new String [n];
	int meatCounter=0;
	int drinkCounter=0;
	int itemsCounter=0;
	for(int i = 0 ; i < n; i++ )		
	{
			drinks[i] = (String) JOptionPane.showInputDialog(null,
					"Choose Drink Size", "Drink", questionMessage,
					null, Items.SIZES, Items.SIZES[0]);
			meats[i] = (String) JOptionPane.showInputDialog(null,
					"Choose type of meats", "Meat", questionMessage,
					null, Items.MEATS, Items.MEATS[0]);
			items[i] = (String) JOptionPane.showInputDialog(null,
					"Choose items", "Items", questionMessage,
					null, Items.ITEMS, Items.ITEMS[0]);
	}
	/**
	 * Calculate the price of the items purchase
	 */
	// calculate price

	double totalCost = 0.0;
	for(int i = 0; i < drinks.length; i++)
	{
		switch(drinks[i])
		{
			case "Small":
				totalCost += Items.smallDrinkPrice;
			 drinkCounter =0;
				break;
			case "Medium":
				totalCost += Items.mediumDrinkPrice;
				drinkCounter= 1;
				break;
			case "Large":
				totalCost += Items.largeDrinkPrice;
				drinkCounter= 2;
				break;
				
			default:
				break;
		}
	}
	for(int i=0; i< meats.length;i++){
		switch(meats[i]){
		case "chicken":
			totalCost += Items.chicken;
			meatCounter= 0;
			break;
		case "steak":
			totalCost += Items.steak;
			meatCounter=1;
			break;
		case "carnitas":
			totalCost += Items.carnitas;
			meatCounter= 2;
			break;
		case "caesar":
			totalCost += Items.caesar;
			meatCounter= 3;
			break;
		case "veggie":
			totalCost += Items.veggie;
			meatCounter=4;
			break;
		default:
			break;
		}
	}
	for(int i=0; i< items.length;i++){
		switch(items[i]){
		case "Tacos":
			totalCost += Items.Tacos;
			itemsCounter= 0;
			break;
		case "Bowls":
			totalCost += Items.Bowls;
			itemsCounter =1;
			break;
		case "Burritos":
			totalCost += Items.Burritos;
			itemsCounter= 2;
			break;
		case "Quesadillas":
			totalCost += Items.Quesadillas;
			itemsCounter=3;
			break;
		case "Salads":
			totalCost += Items.Salads;
			itemsCounter =4;
			break;
		default:
			break;

		}}
	
	ReceiptPrinter RP = new ReceiptPrinter(Items.SIZES, Items.MEATS, Items.ITEMS, totalCost,drinkCounter, meatCounter,itemsCounter,
			meatsPrices, sizesPrices, itemsPrices);
	
	}
	}






