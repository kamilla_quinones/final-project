import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;


public class PosDemo extends JFrame implements ActionListener{
	//initial 
	//start with buttons
	JButton jbtnRegistration = new JButton("Register an account");

	JButton jbtnSignIn = new JButton("Sign in");
/**
 * Construct the POS System
 */
	public PosDemo() {
		jbtnRegistration.addActionListener(this);
		jbtnSignIn.addActionListener(this);

		setLayout(new FlowLayout());
		add(jbtnRegistration);
		add(jbtnSignIn);

		setVisible(true);
		setTitle("POS System");
		setExtendedState(MAXIMIZED_BOTH);
		//add(jbtnRegistration);
	}

	public static void main(String[] args) {
		PosDemo pos = new PosDemo();
		new PosDemo();
	
	}
/**
 * Get actionPerformed
 */
	//Here is were is going to another class
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("Register an account")){
			new Registration();

		}else if (e.getActionCommand().equals("Sign in")) {
			new Authentication();
			
		}

	}
}
